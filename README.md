# Private Key Store protocol

PKS manages keys, unlocks them on demand and allows a limited set of cryptographic operations to be performed using these keys. It does not store cached PIN values and it does not allow (at least for now) enumerating all private keys in store.

It is expected that clients will have hints on which keys are in the store. For example `sq` CLI accepts the `--recipient-key` argument. This key can be inspected to discover subkeys that the user assumes as their own.

Key Store Root is e.g. http://localhost:3000/ or, also using HTTP protocol, unix socket in form of `unix:///run/user/1000/pks`.

Key fingerprint is always appended to the root URL forming the full key URL.

## Accessing unlocked keys

To check if the key is usable issue `POST` to http://localhost:3000/F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1?capability=decrypt with no data in the request body (empty body). If the key is usable the request will return a redirect with `Location` header pointing to the unlocked key.

Note that since key store does *not* cache PINs this method is rarely used. Potential use cases: software keys that are unencrypted and cards that have auto-PIN-unlocking enabled.

## Accessing locked keys

To use a locked key issue `POST` to http://localhost:3000/F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1?capability=decrypt setting the authorization value (e.g. PIN) as the content body: e.g. `123456`. This will make the key store verify if it contains the key and if the PIN is valid.

If all checks succeed the response will contain `Location` header pointing to unlocked key. If the key does not exist the response will be `404 Not Found`. If the PIN value is invalid it will be `403 Forbidden`.

Note that the API can increase the key privacy and return `404` code instead of `403` even on bad PIN scenario.

That URL returned by the store in the `Location` header is a capability allowing its user to perform a cryptographic operation with the key.

Capabilities defined in this specification:
  - `decrypt` - represents RSA decryption and ECDH derivation,
  - `sign` - represents RSA 1.5 signature generation or ECDSA/EdDSA signing.

Additionally to the `Location` header the unlock call can return an `Accept-Post` header with a list of supported operations on the capability URL.

## Decryption

The URL returned in first step can be used for RSA decryption: by `POST` a raw symmetric key bytes to that URL and on success the response will contain raw unencrypted data. The request must contain a `Content-Type` header set to `application/vnd.pks.rsa.ciphertext`.

For ECDH the process is similar: one does POST a ECDH ephemeral point value and retrieves the S parameter. The request must contain a `Content-Type` header set to `application/vnd.pks.ecdh.point`.

## Signing

Signing is similar: digest value is POSTed to the capability URL returned. Due to how PKCS 1.5 RSA signing is structured the store needs to know the digest used. This is communicated using `Content-Type` header.

The following values for that header are defined in this specification:
  - `application/vnd.pks.digest.sha1` - SHA-1,
  - `application/vnd.pks.digest.sha256` - SHA-256,
  - `application/vnd.pks.digest.sha512` - SHA-512.

The application can support other content types. The list of supported types is communicated using `Accept-Post` header.

The response is a raw signature. The following content types have been defined:
  - `application/vnd.pks.signature.rsa` - RSA signature,
  - `application/vnd.pks.signature.eddsa.rs` - R and S values concatenated (64 bytes in total) of the EdDSA signature,
  - `application/vnd.pks.signature.ecdsa.rs` - R and S values concatenated (exact number of bytes varies but the number of bytes is always even and R and S always are of the same size) of the ECDSA signature.

# Extension: public keys

A number of solutions required access to public keys: for example the SSH Agent integration. While Private Key Store's primary function is cryptographic operations and extension has been designed to provide access to raw public key bits.

The content is retrieved using a `GET` request to the key URL (e.g. http://localhost:3000/F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1) with the path segment `/public` appended. E.g. the full URL to the public bits is http://localhost:3000/F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1/public

The following content types can be returned:
  - `application/vnd.pks.public.rsa.modulus` - full RSA modulus of the key. The exponent is assumed to be 65567 unless specified otherwise using the `Exponent` header,
  - `application/vnd.pks.public.ec.compressed` - compressed point on elliptic curve.

# FAQ

Q. Why is the functionality of the private key store so limited? How does one add more keys to it?

A. This specification is limited to client interactions on purpose. From the point of view of the client only two features are visible: unlocking a key and using a key (decryption or signing). All other features are considered an implementation detail of the private key store (they may be specified somewhere else for example).

Q. If key lookup uses OpenPGP fingerprints does it mean the private key store need to compute fingerprints and access OpenPGP certificates?

A. This is an implementation detail of the store. The fingerprints can be treated as "labels" that are assigned to keys (e.g. when using "foreign" keys from TPM or PKCS#11 tokens). Stores that access OpenPGP Cards can use the client supplied fingerprint to lookup connected tokens for a key that matches.

Q. Why HTTP instead of custom protocol?

A. HTTP is widely deployed and virtually all modern programming languages have client libraries for it. HTTP can be routed through unix sockets for local traffic. HTTP can use additional authentication (be it Basic Auth or mutual TLS) for secure, authenticated access to remote stores. HTTP is extensible in case the protocol had to be modified.

Q. The protocol doesn't specify a way to get metadata about the key...?

A. Yes but private key store implementations can add that on top of this protocol for example by returning key info in JSON (not specified here).

Q. Can multiple backends provide access the same OpenPGP key? E.g. the same key in TPM and on a smartcard?

A. When the client asks for a key using OpenPGP fingerprint the private key store implementation can use any logic that it sees fit to select a backing key from available ones. For example the OpenPGP Card store checks all connected cards for matching keys so in effect one query can represent multiple keys from which one is used. The exact details of this process are implementation details of the private key store.

Q. If the protocol is using HTTP does it mean that keys are exposed on the internet?

A. Not necessarily: the private key store can listen on a unix socket to limit local access or - on the other side of the spectrum - it could be a remote service providing authenticated access to keys.

Q. Is it possible to access private or public key bits of stored keys?

A. This specification doesn't define a mechanism to do that. Public bits are assumed to be in possession of the client (e.g. for ECDH mechanism or fingerprint calculation). The private key store protocol just allows unlocking and key access.

Q. The client sends the PIN. Is it secure?

A. The client holding PIN and then the "capability URL" effectively stays in control of the key usage. If the keys were unlocked out-of-band then whoever has access to the private key store socket can use an already unlocked key (this is a case of GPG Agent). Note that by using HTTP (request-response) protocol, it is still technically possible to implement an unlocking mechanism in the private-key-store but this is out of scope for this specification.

Q. The communication uses raw bytes instead of JSON encoded messages. Isn't this severly limiting?

A. The spec mandates bytes because it's all that's necessary. In case the protocol had to be evolved HTTP has enough toggles to allow extension (e.g. Accept and Content-Type headers).

Q. If using HTTP secure? What if a random page accesses private keys?

A. This is not possible due to Cross-Origin Resource Sharing but even if it was possible the page would have to know PIN to unlock a key. Note that private key stores can optionally allow clients to access it via CORS in a controlled manner but this is considered an implementation detail of the store.

